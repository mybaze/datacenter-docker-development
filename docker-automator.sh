#!/bin/bash

# start boot2docker
boot2docker up
$(boot2docker shellinit)

# Start NFS
#sudo echo "nfs.server.mount.require_resv_port = 0" >> /etc/nfs.conf
#sudo echo "/Users -alldirs -mapall=501:20 -network 192.168.59.0 -mask 255.255.255.0" >> /etc/exports
 
sudo nfsd update
sudo nfsd checkexports
showmount -e
sudo nfsd start

boot2docker ssh "sudo /usr/local/etc/init.d/nfs-client start"
boot2docker ssh "sudo umount /Users"
boot2docker ssh "sudo mount 192.168.59.3:/Users /Users -o rw,async,noatime,rsize=32768,wsize=32768,proto=tcp"

docker-compose up
